// Array accessing and manipulation
let getArrOfNumbers = () => {
  let arrayOfNumbers = [];
  let num1 = parseInt(prompt("Enter 1st number: "));
  console.log(`Enter 1st number: ${num1}`);
  arrayOfNumbers[arrayOfNumbers.length] = num1;
  let num2 = parseInt(prompt("Enter 2nd number: "));
  console.log(`Enter 2nd number: ${num2}`);
  arrayOfNumbers[arrayOfNumbers.length] = num2;
  let num3 = parseInt(prompt("Enter 3rd number: "));
  console.log(`Enter 3rd number: ${num3}`);
  arrayOfNumbers[arrayOfNumbers.length] = num3;
  let num4 = parseInt(prompt("Enter 4th number: "));
  console.log(`Enter 4th number: ${num4}`);
  arrayOfNumbers[arrayOfNumbers.length] = num4;
  let num5 = parseInt(prompt("Enter 5th number: "));
  console.log(`Enter 5th number: ${num5}`);
  arrayOfNumbers[arrayOfNumbers.length] = num5;

  let total = 0;
  for (let i = 0; i < arrayOfNumbers.length; i++) {
    total += arrayOfNumbers[i];
  }
  alert(`The total of the entered five number is ${total}`);
};

let lookForDuplicate = () => {
  let fruits = [
    "apple",
    "grapes",
    "oranges",
    "mango",
    "apple",
    "strawberry",
    "grapes",
  ];
  let foundDuplicate = fruits.filter(
    (fruit, index) => fruits.indexOf(fruit) !== index
  );

  console.log(foundDuplicate);
};

//ES6 Updates
let numbers = [2, 4, 6, 8, 10];

const cube = numbers.map((num) => num ** 3);

for (let i = 0; i < numbers.length; i++) {
  console.log(`The cube value of ${numbers[i]} is ${cube[i]}`);
}
